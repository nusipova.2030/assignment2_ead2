package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;

@WebFilter("/LoggingFilter")
public class LoggingFilter implements Filter {
    private ServletContext context;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.context = filterConfig.getServletContext();
        System.out.println("Request Logging Filter!!!");

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        Enumeration<String> params = req.getHeaderNames();

        while (params.hasMoreElements()){
            String username = params.nextElement();
            String value = servletRequest.getParameter(username);

            System.out.println(req.getRemoteAddr()+ "::Request Params::{"+username+"="+value+"}");

            Cookie[] cookies = req.getCookies();
            if(cookies !=null) {
                for (Cookie cookie : cookies) {
                    System.out.println(req.getRemoteAddr() + "::Cookie::{"+cookie.getName()+","+cookie.getValue()+"}");
                }
            }
        }


        filterChain.doFilter(servletRequest,servletResponse);

    }

    @Override
    public void destroy() {

    }
}
